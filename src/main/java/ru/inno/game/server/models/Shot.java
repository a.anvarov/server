package ru.inno.game.server.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Shot {
    private Long id;
    private Player shooter;
    private Player target;
    private Game game;

    public Shot(Player shooter, Player target, Game game) {
        this.shooter = shooter;
        this.target = target;
        this.game = game;
    }
}
