package ru.inno.game.server.models;

import lombok.*;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Player {
    private Long id;
    private String ip;
    private String nickName;
    private Integer maxPoint;
    private Integer winLoseCount;

    public Player(String nickName) {
        this.nickName = nickName;
    }

    public Player(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(nickName, player.nickName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickName);
    }
}
