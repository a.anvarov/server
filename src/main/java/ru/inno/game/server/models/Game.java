package ru.inno.game.server.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Game {

    private Long id;
    private String gameDate;
    private Player first;
    private Player second;
    private Long gameTime;
    private Integer firstPlayerShotsCount = 0;
    private Integer secondPlayerShotsCount = 0;
}
