package ru.inno.game.server.services;

import lombok.extern.log4j.Log4j2;
import ru.inno.game.server.models.Game;
import ru.inno.game.server.models.Player;
import ru.inno.game.server.models.Shot;
import ru.inno.game.server.repositories.GamesRepository;
import ru.inno.game.server.repositories.PlayersRepository;
import ru.inno.game.server.repositories.ShotsRepository;

import java.time.LocalDate;

@Log4j2
public class TanksServiceImpl implements TanksService {

    private ShotsRepository shotsRepository;
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;

    public TanksServiceImpl(ShotsRepository shotsRepository, PlayersRepository playersRepository, GamesRepository gamesRepository) {
        this.shotsRepository = shotsRepository;
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
    }

    public Long startGame(String firstPlayerNickname, String firstPlayerIp, String secondPlayerNickname, String secondPlayerIp) {
        Player firstPlayer = playersRepository.findByNickname(firstPlayerNickname);
        Player secondPlayer = playersRepository.findByNickname(secondPlayerNickname);
        // если пользователи новые
        if (firstPlayer == null) {
            firstPlayer = new Player(firstPlayerNickname);
            firstPlayer = playersRepository.save(firstPlayer);
        } else {
            firstPlayer.setIp(firstPlayerIp);
            playersRepository.update(firstPlayer);
        }

        if (secondPlayer == null) {
            secondPlayer = new Player(secondPlayerNickname);
            secondPlayer = playersRepository.save(secondPlayer);
        } else {
            secondPlayer.setIp(secondPlayerIp);
            playersRepository.update(secondPlayer);
        }

        Game game = new Game();
        game.setFirst(firstPlayer);
        game.setSecond(secondPlayer);
        game.setGameDate(LocalDate.now().toString());

        game = gamesRepository.save(game);
        return game.getId();
    }

    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Player first = playersRepository.findById(game.getFirst().getId());
        Player second = playersRepository.findById(game.getSecond().getId());

        game.setFirst(first);
        game.setSecond(second);

        Shot shot = new Shot(shooter, target, game);

        if (game.getFirst().equals(shooter)) {
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        } else if (game.getSecond().equals(shooter)) {
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }
        shotsRepository.save(shot);
        gamesRepository.update(game);

        log.debug(" shots: {} | {} ", game.getFirstPlayerShotsCount(), game.getSecondPlayerShotsCount());
    }
}
