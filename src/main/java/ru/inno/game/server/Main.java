package ru.inno.game.server;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import ru.inno.game.server.config.HikariConfigurations;
import ru.inno.game.server.repositories.*;
import ru.inno.game.server.server.GameServer;
import ru.inno.game.server.services.TanksServiceImpl;

@Log4j2
public class Main {

    public static void main(String[] args) {
        HikariConfigurations hikariConfigurations = new HikariConfigurations();

        HikariDataSource hikariDataSource = hikariConfigurations.dataSource();

        log.debug("Initialize DataSource - HikariDataSource!");

        GamesRepository gamesRepository = new GamesRepositoryImpl(hikariDataSource);

        log.debug("Initialize GamesRepository!");

        PlayersRepository playersRepository = new PlayersRepositoryImpl(hikariDataSource);

        log.debug("Initialize PlayersRepository!");

        ShotsRepository shotsRepository = new ShotsRepositoryImpl(hikariDataSource);

        log.debug("Initialize ShotsRepository!");

        GameServer gameServer = new GameServer(new TanksServiceImpl(shotsRepository, playersRepository, gamesRepository));

        log.debug("Initialize GameServer!");

        gameServer.start(7777);
    }
}
