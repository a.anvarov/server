package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Player;

public interface PlayersRepository extends CrudRepository<Player> {
    Player findByNickname(String nickname);
    Player save(Player firstPlayer);
    void update(Player player);
    Player findById(Long id);
}
