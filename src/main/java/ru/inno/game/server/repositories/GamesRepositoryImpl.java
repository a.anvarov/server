package ru.inno.game.server.repositories;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import ru.inno.game.server.models.Game;
import ru.inno.game.server.models.Player;

import java.sql.*;

@Log4j2
public class GamesRepositoryImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_SAVE_GAME = "insert into game (game_date, first, second) values (?, ?, ?);";

    //language=SQL
    private static final String SQL_SELECT_GAME_BY_ID = "select id, game_date, first, second, game_time, first_player_shoots_count, second_player_shots_count from game where id = ?;";

    //language=SQL
    private static final String SQL_UPDATE_GAME = "update game set game_date = ?, first = ?, second = ?, game_time = ?, first_player_shoots_count = ?, second_player_shots_count = ?  where id = ?;";

    private HikariDataSource hikariDataSource;

    public GamesRepositoryImpl(HikariDataSource hikariDataSource) {
        this.hikariDataSource = hikariDataSource;
    }

    public Game save(Game game) {
        log.debug("Игра началась для игроков {} {} в {} ", game.getFirst().getNickName(), game.getSecond().getNickName(), game.getGameDate());
        try (Connection connection = hikariDataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_SAVE_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getGameDate());
            statement.setLong(2, game.getFirst().getId());
            statement.setLong(3, game.getSecond().getId());
            statement.executeUpdate();

            // generatedIds - множество строк, которое содержит сгенерированные ключи после выполнения запроса
            ResultSet generatedIds = statement.getGeneratedKeys();
            // next() возвращает true, если в generatedIds что-то есть, если там ничего нет - значит что-то пошло не такх
            if (generatedIds.next()) {
                // из generatedIds получаем сгенерированный ключ с названием id
                int generatedId = generatedIds.getInt("id");
                // для сохраняемой сущности проставляем id, который получили для этой сущности из базы
                game.setId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug(e.getMessage());
        }
        return game;
    }

    public Game findById(Long gameId) {
        Game game = null;
        try (Connection connection = hikariDataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_SELECT_GAME_BY_ID)) {
            statement.setLong(1, gameId);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                game = gameRowMapper.mapRow(result);
            }
            result.close();
            return game;
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug(e.getMessage());
        }
        return game;
    }

    public void update(Game game) {
        try (Connection connection = hikariDataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME)) {
            statement.setString(1, game.getGameDate());
            statement.setLong(2, game.getFirst().getId());
            statement.setLong(3, game.getSecond().getId());
            statement.setDate(4, game.getGameTime() != null ? new Date(game.getGameTime()) : null);
            statement.setInt(5, game.getFirstPlayerShotsCount());
            statement.setInt(6, game.getSecondPlayerShotsCount());
            statement.setLong(7, game.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug(e.getMessage());
        }
    }

    private RowMapper<Game> gameRowMapper = row ->
            Game.builder()
                    .id((long) row.getInt("id"))
                    .gameDate(row.getString("game_date"))
                    .first(new Player(row.getLong("first")))
                    .second(new Player(row.getLong("second")))
                    .gameTime(row.getDate("game_time") != null ? row.getDate("game_time").getTime() : null)
                    .firstPlayerShotsCount(row.getInt("first_player_shoots_count"))
                    .secondPlayerShotsCount(row.getInt("second_player_shots_count"))
                    .build();
}
