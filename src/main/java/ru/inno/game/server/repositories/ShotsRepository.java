package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Shot;

public interface ShotsRepository extends CrudRepository<Shot> {
    Shot save(Shot shot);
}
