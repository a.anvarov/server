package ru.inno.game.server.repositories;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import ru.inno.game.server.models.Shot;

import java.sql.*;

@Log4j2
public class ShotsRepositoryImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_SAVE_SHOT = "insert into shoot (shooter, target, game_id) values (?, ?, ?);";

    private HikariDataSource hikariDataSource;

    public ShotsRepositoryImpl(HikariDataSource hikariDataSource) {
        this.hikariDataSource = hikariDataSource;
    }

    public Shot save(Shot shot) {
        log.debug("ShootRepository - saved {} {} with game id = {}  ", shot.getShooter().getNickName(), shot.getTarget().getNickName(), shot.getGame().getId());
        try (Connection connection = hikariDataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_SAVE_SHOT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, shot.getShooter().getId());
            statement.setLong(2, shot.getTarget().getId());
            statement.setLong(3, shot.getGame().getId());
            statement.executeUpdate();

            // generatedIds - множество строк, которое содержит сгенерированные ключи после выполнения запроса
            ResultSet generatedIds = statement.getGeneratedKeys();
            // next() возвращает true, если в generatedIds что-то есть, если там ничего нет - значит что-то пошло не такх
            if (generatedIds.next()) {
                // из generatedIds получаем сгенерированный ключ с названием id
                int generatedId = generatedIds.getInt("id");
                // для сохраняемой сущности проставляем id, который получили для этой сущности из базы
                shot.setId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug(e.getMessage());
        }
        return shot;
    }
}
