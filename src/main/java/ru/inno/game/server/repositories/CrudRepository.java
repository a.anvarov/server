package ru.inno.game.server.repositories;

public interface CrudRepository<T> {
    T save(T t);
    T findById(Long gameId);
    void update(T game);
}
