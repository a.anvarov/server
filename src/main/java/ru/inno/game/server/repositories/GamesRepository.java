package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Game;

public interface GamesRepository extends CrudRepository<Game> {
    Game save(Game game);
    Game findById(Long gameId);
    void update(Game game);
}
