package ru.inno.game.server.repositories;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import ru.inno.game.server.models.Player;

import java.sql.*;

@Log4j2
public class PlayersRepositoryImpl implements PlayersRepository {

    //language=SQL
    private static final String SQL_SAVE_PLAYER = "insert into player (nick_name) values (?);";

    //language=SQL
    private static final String SQL_SELECT_PLAYER_BY_NICK_NAME = "select id, ip, nick_name, max_point, win_lose_count from player where nick_name = ?;";

    //language=SQL
    private static final String SQL_UPDATE_IP = "update player set ip = ? where id = ?;";

    //language=SQL
    private static final String SQL_SELECT_PLAYER_BY_ID = "select id, ip, nick_name, max_point, win_lose_count from player where id = ?;";

    private HikariDataSource hikariDataSource;

    public PlayersRepositoryImpl(HikariDataSource hikariDataSource) {
        this.hikariDataSource = hikariDataSource;
    }

    public Player findByNickname(String nickname) {
        Player player = null;
        try (Connection connection = hikariDataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PLAYER_BY_NICK_NAME)) {
            statement.setString(1, nickname);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                player = playerRowMapper.mapRow(result);
            }
            result.close();
            return player;
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug(e.getMessage());
        }
        return player;
    }

    public Player save(Player player) {
        try (Connection connection = hikariDataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_SAVE_PLAYER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getNickName());
            statement.executeUpdate();

            // generatedIds - множество строк, которое содержит сгенерированные ключи после выполнения запроса
            ResultSet generatedIds = statement.getGeneratedKeys();
            // next() возвращает true, если в generatedIds что-то есть, если там ничего нет - значит что-то пошло не такх
            if (generatedIds.next()) {
                // из generatedIds получаем сгенерированный ключ с названием id
                int generatedId = generatedIds.getInt("id");
                // для сохраняемой сущности проставляем id, который получили для этой сущности из базы
                player.setId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug(e.getMessage());
        }
        return player;
    }

    @Override
    public void update(Player player) {
        try (Connection connection = hikariDataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_IP)) {
            statement.setString(1, player.getIp());
            statement.setInt(2, player.getId().intValue());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug(e.getMessage());
        }
    }

    public Player findById(Long id) {
        Player player = null;
        try (Connection connection = hikariDataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PLAYER_BY_ID)) {
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                player = playerRowMapper.mapRow(result);
            }
            result.close();
            return player;
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug(e.getMessage());
        }
        return player;
    }

    private RowMapper<Player> playerRowMapper = row ->
            Player.builder()
                    .id((long) row.getInt("id"))
                    .ip(row.getString("ip"))
                    .nickName(row.getString("nick_name"))
                    .maxPoint(row.getInt("max_point"))
                    .winLoseCount(row.getInt("win_lose_count"))
                    .build();
}
