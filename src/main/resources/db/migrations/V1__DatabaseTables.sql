
create table player
(
    id             bigserial primary key,
    ip             varchar(16),
    nick_name      varchar(20),
    max_point      int,
    win_lose_count int
);

create table game
(
    id                        bigserial primary key,
    game_date                 varchar,
    first                     bigint,
    second                    bigint,
    game_time                 timestamp,
    first_player_shoots_count bigint,
    second_player_shots_count bigint,
    foreign key (first) references player (id),
    foreign key (second) references player (id)
);


create table shoot
(
    id      bigserial primary key,
    shooter bigint,
    target  bigint,
    game_id bigint,
    foreign key (shooter) references player (id),
    foreign key (target) references player (id),
    foreign key (game_id) references game (id)
);